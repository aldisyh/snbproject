<?php

namespace App\Http\Middleware;

use Closure;
use App\Model\RoleUsers;
use Illuminate\Support\Facades\Auth;

class MemberMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */

    public function handle($request, Closure $next)
    {
        if (!empty(auth()->user())) {
            $access = RoleUsers::where('user_id', auth()->user()->id)->first();
            if (!empty($access) && ($access->nm_role == 'member')) {
                return $next($request);
            } else {
                return redirect()->route('member.form')->with('gagal', 'Password Salah');
            }
        } else {
            return redirect()->route('member.form')->with('gagal', 'Password Salah');
        }
        return $next($request);
    }
}
