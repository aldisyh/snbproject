<?php

namespace App\Http\Controllers\Admin\Post;

use App\Http\Controllers\Controller;
use App\Model\ProjectDetail;
use App\Model\AirdropDetail;
use App\Model\Participants;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $datas = ProjectDetail::whereDate('created_at', '=', $request->from_date)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $datas = ProjectDetail::whereBetween('created_at', array($request->from_date, $request->to_date))->get();
                }
            }
            //load data default
            else {
                $datas = ProjectDetail::orderBy('created_at', 'DESC')->get();
            }

            return Datatables()->of($datas)
                ->addColumn('image', function ($data) {
                    $url = asset('storage/images/' . $data->logo);
                    return '<img src="' . $url . '" border="0" width="70" class="img-rounded" align="center" />';

                    // $url = '. Storage::Disk("s3")->url("public/images/ . $data->image") .';
                    // return '<img src=' . $url . ' border="0" width="40" align="center" />';

                    // $image = '<src="{{Storage::disk('s3')->url('public/images/product/'. $sub2['img']['img_product'])}}" width="320px" height="340px">';
                    // return $image;
                })
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("post.detail", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Show</a>';
                    $button .= '&nbsp;&nbsp;';
                    $button .= '<a href="' . route("post.edit", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="delete btn btn-info btn-sm"><i class="far fa-trash-alt"> Edit Post</a>';
                    return $button;
                })
                ->rawColumns(['status_label', 'action'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view('admin.post.index');
    }

    public function form()
    {
        return view('admin.post.edit_form');
    }

    public function tambah(Request $request)
    {
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/images', $filename);

            $post = ProjectDetail::create([
                'title' => $request->title,
                'description' => $request->description,
                'website' => $request->website,
                'telegram' => $request->telegram,
                'twitter' => $request->twitter,
                'logo' => $filename,
            ]);
        }

        $post_id = $post->id;

        $airdrop = AirdropDetail::create([
            'airdrop_link' => $request->airdrop_link,
            'airdrop_starts' => $request->airdrop_starts,
            'airdrop_ends' => $request->airdrop_ends,
            'airdrop_distribution' => $request->airdrop_distribution,
        ]);

        $airdrop_id = $airdrop->id;

        $airdrop = Participants::create([
            'rewards' => $request->rewards,
            'referral_bonus' => $request->referral_bonus,
            'maximum_participant' => $request->maximum_participant,
            'value_rewards' => $request->value_rewards,
            'value_referral'=> $request->value_referral
        ]);

        return redirect(route('post.index'))->with(['success' => 'Create Post Has Been Success']);
    }

    public function edit($id)
    {
        $post = ProjectDetail::where(['id' => $id])->first();
        $airdrop = AirdropDetail::where(['project_id' => $post->id])->first();
        $participants = Participants::where(['airdrop_id' => $airdrop->id])->first();

        return view('admin.post.edit_form', compact('post', 'airdrop', 'participants'));
    }

    public function update(Request $request, $id) {

        $post = ProjectDetail::where(['id' => $id])->first();
        $airdrop = AirdropDetail::where(['project_id' => $post->id])->first();
        $participants = Participants::where(['airdrop_id' => $airdrop->id])->first();

        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/images', $filename);
        } else {
            $filename = $post->logo;
        }

        $post->update([
            'title' => $request->title,
            'description' => $request->description,
            'website' => $request->website,
            'telegram' => $request->telegram,
            'twitter' => $request->twitter,
            'logo' => $filename,
            'slug'=> $this->slug_generated($request->title)
        ]);

        $airdrop->update([
            'airdrop_link' => $request->airdrop_link,
            'airdrop_starts' => $request->airdrop_starts,
            'airdrop_ends' => $request->airdrop_ends,
            'airdrop_distribution' => $request->airdrop_distribution
        ]);

        $participants->update([
            'rewards' => $request->rewards,
            'referral_bonus' => $request->referral_bonus,
            'maximum_participant' => $request->maximum_participant,
            'value_rewards' => $request->value_rewards,
            'value_referral'=> $request->value_referral
        ]);

        return redirect(route('post.index'))->with(['success' => 'Post has Been Updated']);
    }

    public function detail_post($id)
    {
        $post = ProjectDetail::find($id);

        $post['airdrop'] = AirdropDetail::where(['project_id' => $post->id])->first();
        $post['participant'] = Participants::where(['airdrop_id' => $post->id])->first();

        return view('admin.post.detail_post', compact('post'));
    }

    public function slug_generated($string){
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
        return $slug;
    }

    public function publish($id)
    {
        $order = ProjectDetail::where(['id' => $id])->first();

        $order->update(['status' => 1]);

        return redirect()->back()->with('success', 'Success');
    }

    public function draft($id)
    {
        $order = ProjectDetail::where(['id' => $id])->first();

        $order->update(['status' => 2]);

        return redirect()->back()->with('success', 'Success');
    }

    public function delete($id)
    {
        $order = ProjectDetail::where(['id' => $id])->first();

        $order->update(['status' => 3]);

        return redirect()->back()->with('success', 'Success');
    }

    public function new($id)
    {
        $order = ProjectDetail::where(['id' => $id])->first();

        $order->update(['is_status' => 2]);

        return redirect()->back()->with('success', 'Success');
    }

    public function featured($id) //hot
    {
        $order = ProjectDetail::where(['id' => $id])->first();

        $order->update(['is_status' => 1]);

        return redirect()->back()->with('success', 'Success');
    }

    public function ended($id)
    {
        $order = ProjectDetail::where(['id' => $id])->first();

        $order->update(['is_status' => 3]);

        return redirect()->back()->with('success', 'Success');
    }
}
