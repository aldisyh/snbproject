<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ProjectDetail;
use App\Model\AirdropDetail;
use App\Model\Participants;
use App\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('admin');
    }

    public function index()
    {
        $project_all = ProjectDetail::count();
        $project_pending = ProjectDetail::where(['status' => 0])->count();
        $project_approve = ProjectDetail::where(['status' => 1])->count();

        return view('admin.home', compact('project_all', 'project_pending', 'project_approve'));
    }
}
