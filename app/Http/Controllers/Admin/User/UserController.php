<?php

namespace App\Http\Controllers\Admin\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\RoleUsers;

class UserController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $datas = RoleUsers::whereDate('created_at', '=', $request->from_date)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $datas = RoleUsers::whereBetween('created_at', array($request->from_date, $request->to_date))->get();
                }
            }
            //load data default
            else {
                $datas = RoleUsers::orderBy('created_at', 'DESC')->get();
            }

            return Datatables()->of($datas)
                ->addColumn('action', function ($order) {
                    // $button = '<a href="' . route("sales.show", $order->invoice) . '" data-toggle="tooltip"  data-id="' . $order->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Show</a>';
                    // $button .= '&nbsp;&nbsp;';
                    // $button .= '<a href="' . route("confirm.payment", $order->invoice) . '" data-toggle="tooltip"  data-id="' . $order->id . '" data-original-title="Edit" class="delete btn btn-info btn-sm"><i class="far fa-trash-alt"> Bukti Pembayaran</a>';
                    // return $button;
                })
                ->rawColumns(['action', 'status_label', 'address'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        return view('admin.user.index');
    }
}
