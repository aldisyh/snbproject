<?php

namespace App\Http\Controllers\Landing;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ProjectDetail;
use App\Model\AirdropDetail;
use App\Model\Participants;
use Carbon;

class HomeController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $project_all = ProjectDetail::where('status',1)
                ->whereIn('is_status', [1,2,3] )->orderBy('is_status','ASC')->paginate(8);
        $now_date = date("Y-m-d");
        return view('landing.home',compact('project_all','now_date'));
    }
    
    public function detail_airdrop($slug)
    {  
        $project_all = ProjectDetail::where(['status'=>1])
                    ->where('slug', 'like', '%' . $slug . '%')
                    ->first();
        $detail = AirdropDetail::where('project_id',$project_all->id)->first();
        $participant = Participants::where('airdrop_id',$detail->id)->first();
        return view('landing.detail_airdrop',compact('project_all','detail','participant'));
    }


    public function update_koin()
    {
        
    }
}
