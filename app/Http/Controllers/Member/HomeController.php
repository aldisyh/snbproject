<?php

namespace App\Http\Controllers\Member;

use App\Http\Controllers\Member\Controller;
use Illuminate\Http\Request;
use App\Model\ProjectDetail;
use App\Model\AirdropDetail;
use App\Model\Participants;
use App\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('member');

        $this->middleware(
            function ($request, $next) {
                $this->user = User::where(['id' => auth()->user()->id])->first();

                return $next($request);
            }
        );
    }

    public function index()
    {
        $project_all = ProjectDetail::where(['user_id' => auth()->user()->id])->count();
        $project_pending = ProjectDetail::where(['user_id' => auth()->user()->id, 'status' => 0])->count();
        $project_approve = ProjectDetail::where(['user_id' => auth()->user()->id, 'status' => 1])->count();

        $user = $this->user;

        return view('member.home', compact('project_all', 'project_pending', 'project_approve', 'user'));
    }
}
