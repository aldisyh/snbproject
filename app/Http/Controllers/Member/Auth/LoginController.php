<?php

namespace App\Http\Controllers\Member\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Model\AccessRole;
use App\Model\Country;
use Mail;
use Carbon;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('guest')->except('logout');
    }

    public function formlogin()
    {
        return view('member.auth.login');
    }

    public function postLogin(Request $request)
    {
        if (!Auth::attempt([
            'email' => $request->email,
            'password' => $request->password
        ])) {
            return redirect()->back();
        }
        return redirect()->route('member.dashboard');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('member.form');
    }

    public function formregister()
    {
        $countrys = Country::all();

        return view('member.auth.register', compact('countrys'));
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'country_id' => 'required',
        ]);

        $token_confirm = rand(1000000, 1000000000);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'country_id' => (int)$request->country_id,
            'password' => bcrypt($request->password),
            'token' => $token_confirm
        ]);

        AccessRole::create([
            'role_id' => 2, //buyer
            'user_id' => $user->id
        ]);

        $url_token = env('APP_URL')."/auth/email/verify/".base64_encode($token_confirm);
        $to_name = $request->name;
        $to_email = $request->email;
        $data = array('name'=>$request->name, "body" => "Welcome to Airdrops Media","token"=>$url_token);

        Mail::send('emails.mail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Welcome Airdrops Media');
            $message->from("no-reply@airdropsmedia.com",'Airdropsmedia');
        });

        return redirect(route('register'))->with(['success' => 'Register Successfull']);
    }

    public function verify_email($token){
        $token = base64_decode($token);

        $update = User::where('token',$token)->update([
            'token' => null,
            'email_verified_at' => Carbon\Carbon::now()
        ]);

        if (!empty($update)) {
            return redirect(route('member.login'))->with(['success' => 'Varify Email Successfull']);
        } else {
            return redirect(route('member.login'))->with(['failed' => 'Varify Email Failed']);
        }
    }

    public function forgot_email()
    {
        return view('auth.forgot');
    }
    
    public function post_forgot_email(Request $request)
    {
        $token_confirm = rand(1000000, 1000000000);
        $url_token = env('APP_URL')."/auth/email/forgot/".base64_encode($token_confirm);

        $account = User::where('email',$request->email)->first();
        $account->update([
            'token' => $token_confirm
        ]);

        $to_email = $account->email;
        $to_name = $account->name;

        $data = array('name'=> $account->name, "body" => "Forgot Account","token"=>$url_token);

        Mail::send('emails.forgot_mail', $data, function($message) use ($to_name, $to_email) {
            $message->to($to_email, $to_name)
                    ->subject('Forgot Account - Airdrops Media');
            $message->from("no-reply@airdropsmedia.com",'Airdrops Media');
        });

        return redirect(route('login'))->with(['success' => 'Please Check your email']);
    }
    
    public function forgot_email_reset($token){    
        $token = base64_decode($token);
        $account = User::where('token',$token)->first();
        $id_account = $account->id;

        if (!empty($account)) {
            return view('auth.reset',compact('id_account'));
        } else {
            return redirect(route('login'))->with(['failed' => 'Check Account Failed']);
        }
    }

    public function reset_account(Request $request, $id){
        $this->validate($request, [
            'password' => 'required|string|confirmed|min:6',
        ]);

        $user = User::where('id', $request->id)
            ->update([
                'password' => bcrypt($request->get('password')),
                'token' => null
            ]);

        return redirect(route('login'))->with(['success' => 'Reset Password Success']);
    }
}
