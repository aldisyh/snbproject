<?php

namespace App\Http\Controllers\Member\Post;

use App\Http\Controllers\Member\Controller;
use App\Model\ProjectDetail;
use App\Model\AirdropDetail;
use App\Model\Participants;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            if (!empty($request->from_date)) {
                //Jika tanggal awal(from_date) hingga tanggal akhir(to_date) adalah sama maka
                if ($request->from_date === $request->to_date) {
                    //kita filter tanggalnya sesuai dengan request from_date
                    $datas = ProjectDetail::where(['user_id' => auth()->user()->id])->whereDate('created_at', '=', $request->from_date)->get();
                } else {
                    //kita filter dari tanggal awal ke akhir
                    $datas = ProjectDetail::where(['user_id' => auth()->user()->id])->whereBetween('created_at', array($request->from_date, $request->to_date))->get();
                }
            }
            //load data default
            else {
                $datas = ProjectDetail::where(['user_id' => auth()->user()->id])->orderBy('created_at', 'DESC')->get();
            }

            return Datatables()->of($datas)
                ->addColumn('image', function ($data) {
                    $url = asset('storage/images/' . $data->logo);
                    return '<img src="' . $url . '" border="0" width="70" class="img-rounded" align="center" />';

                    // $url = '. Storage::Disk("s3")->url("public/images/ . $data->image") .';
                    // return '<img src=' . $url . ' border="0" width="40" align="center" />';

                    // $image = '<src="{{Storage::disk('s3')->url('public/images/product/'. $sub2['img']['img_product'])}}" width="320px" height="340px">';
                    // return $image;
                })
                ->addColumn('action', function ($data) {
                    $button = '<a href="' . route("detail.post", $data->id) . '" data-toggle="tooltip"  data-id="' . $data->id . '" data-original-title="Edit" class="edit btn btn-primary btn-sm edit-post"><i class="far fa-edit"></i> Show</a>';
                    return $button;
                })
                ->rawColumns(['status_label', 'action'])
                ->addIndexColumn()
                ->escapeColumns([])
                ->make(true);
        }

        $user = $this->user;


        return view('member.post.index', compact('user'));
    }

    public function form()
    {
        $user = $this->user;

        return view('member.post.form', compact('user'));
    }

    public function tambah(Request $request)
    {
        if ($request->hasFile('logo')) {
            $file = $request->file('logo');
            $filename = time() . ($request->name) . '.' . $file->getClientOriginalExtension();
            $file->storeAs('public/images', $filename);

            $post = ProjectDetail::create([
                'user_id' => auth()->user()->id,
                'title' => $request->title,
                'slug'=> $this->slug_generated($request->title),
                'description' => $request->description,
                'website' => $request->website,
                'telegram' => $request->telegram,
                'twitter' => $request->twitter,
                'logo' => $filename,
                'status' => 0,
            ]);
        }

        $post_id = $post->id;

        $airdrop = AirdropDetail::create([
            'project_id' => $post_id,
            'airdrop_link' => $request->airdrop_link,
            'airdrop_starts' => $request->airdrop_starts,
            'airdrop_ends' => $request->airdrop_ends,
            'airdrop_distribution' => $request->airdrop_distribution,
        ]);

        $airdrop_id = $airdrop->id;

        $airdrop = Participants::create([
            'airdrop_id' => $airdrop_id,
            'rewards' => $request->rewards,
            'referral_bonus' => $request->referral_bonus,
            'maximum_participant' => $request->maximum_participant,
            'value_rewards' => $request->value_rewards,
            'value_referral'=> $request->value_referral
        ]);

        return redirect(route('postingan.index'))->with(['success' => 'Success']);
    }

    public function postdetail($id)
    {
        $post = ProjectDetail::find($id);
        $post['airdrop'] = AirdropDetail::where(['project_id' => $post->id])->first();
        $post['participant'] = Participants::where(['airdrop_id' => $post->id])->first();

        $user = $this->user;

        return view('member.post.detail_post', compact('post', 'user'));
    }

    public function slug_generated($string){
        $slug = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $string)));
        return $slug;
    }

}
