<?php

namespace App\Http\Controllers\Member\Profile;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class ProfileController extends Controller
{
    public function index()
    {
        $user = User::where(['id' => auth()->user()->id])->with('country')->first();

        return view('member.profile.index', compact('user'));
    }

    public function changepassword(Request $request)
    {
        $user = User::where('id', $request->id)
            ->update([
                'name' =>  $request->name,
                'email' => $request->email,
                'password' => bcrypt($request->get('password')),
            ]);

        return redirect()->back()->with(['success' => 'Change Success']);
    }
}
