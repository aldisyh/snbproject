<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Model\RoleUsers;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    //protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('guest')->except('logout');
    }

    public function formlogin()
    {
        return view('auth.login');
    }

    public function postLogin(Request $request)
    {
        if (!Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect()->route('member.form')->with('failed', 'Failed Login');
        } else {
            $account = User::where('email', $request->email)->first();
            if (empty($account->email_verified_at) || $account->email_verified_at ==""){
                return redirect()->route('member.form')->with('failed', 'Please Verify your Email');
            } else {
                $access = RoleUsers::where('user_id', auth()->user()->id)->first();
                if (!empty($access) && ($access->nm_role == 'admin')) {
                    return redirect()->route('admin.home')->with('success', 'Login Success');
                } else {
                    return redirect()->route('member.dashboard')->with('failed', 'Login Success');
                }
            }
        }
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route('member.form');
    }
}
