<?php

namespace App\Model;

use Carbon\Carbon;

use Illuminate\Database\Eloquent\Model;

class ProjectDetail extends Model
{
    protected $table = 'project_detail';
    protected $primaryKey = 'id';
    protected $guarded = [''];
    protected $appends = ['status_label'];

    public function getStatusLabelAttribute()
    {
        if ($this->status == 0) {
            return '<span class="badge badge-warning">Pending</span>';
        } elseif ($this->status == 1) {
            return '<span class="badge badge-success">Publish</span>';
        } elseif ($this->status == 2) {
            return '<span class="badge badge-secondary">Draft</span>';
        } elseif ($this->status == 3) {
            return '<span class="badge badge-danger">Delete</span>';
        }
        return 'Selesai';
    }

    public function getCreatedAtAttribute($value)
    {
        return Carbon::parse($value)->format('d-m-Y');
    }

    public function airdrop()
    {
        return $this->hasMany('App\Model\AirdropDetail', 'project_id');
    }
}
