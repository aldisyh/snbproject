<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Participants extends Model
{
    protected $table = 'participants';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function project()
    {
        return $this->belongsTo('App\Model\AirdropDetail', 'airdrop_id');
    }
}
