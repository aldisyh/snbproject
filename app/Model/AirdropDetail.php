<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class AirdropDetail extends Model
{
    protected $table = 'airdrop_detail';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function project()
    {
        return $this->belongsTo('App\Model\ProjectDetail', 'airdrop_id');
    }

    public function participant()
    {
        return $this->hasMany('App\Model\Participants', 'airdrop_id');
    }
}
