<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = 'countries';
    protected $primaryKey = 'id';
    protected $guarded = [''];

    public function user()
    {
        return $this->hasMany('App\User');
    }
}
