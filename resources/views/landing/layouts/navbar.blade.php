<header id="header" class="" data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'false', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
    <div class="header-body border-0">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="{{url('/')}}">
                                <img alt="Porto Finance" height="45" src="https://airdrops.media/img/core-img/safenebula-logo2.png">
                            </a>
                        </div>
                        <div class="nav-brand ">Airdrops Media</div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-links order-2 order-lg-1">
                            <div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                <nav class="collapse">
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li>
                                            <a class="nav-link active" href="{{url('/')}}">
                                                AIRDROPS
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="#" data-toggle="modal" data-target="#no_link">
                                                ICO
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="#" data-toggle="modal" data-target="#no_link">
                                                AMA
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="#" data-toggle="modal" data-target="#no_link">
                                                INCUBATOR
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="#" data-toggle="modal" data-target="#no_link">
                                                RANK
                                            </a>
                                        </li>
                                        <li>
                                            <a class="nav-link" href="#" data-toggle="modal" data-target="#no_link">
                                                FORUM
                                            </a>
                                        </li>

                                        {{-- DEKSTOP --}}
                                        <li class="d-none d-md-block">
                                            <div class="order-1 order-lg-2 ml-2 mt-4 d-none d-md-block">
                                                <a class="btn btn-outline btn-rounded btn-info" type="button" href="{{url('auth/login')}}">Login</a>
                                            </div>
                                        </li>
                                        <li class="d-none d-md-block">
                                            <div class="order-1 order-lg-2 ml-2 mt-4 d-none d-md-block">
                                                <a class="btn btn-outline btn-rounded btn-info active" type="button" href="{{url('auth/form-register')}}">Register</a>
                                            </div>
                                        </li>

                                        {{-- MOBILE --}}
                                        <li class="d-sm-none d-xs-none">
                                            <div class="order-1 order-lg-2 ml-2 mt-4 d-sm-none d-xs-none">
                                                <a class="btn btn-outline btn-rounded btn-info " type="button" href="{{url('auth/login')}}">Login </a>
                                                <a class="btn btn-outline btn-rounded btn-info ml-3 active" type="button" href="{{url('auth/form-register')}}">Register</a>
                                            </div>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>


<!-- Modal -->
<div class="modal fade" id="no_link" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Coming Soon</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        Sorry, Menu is Coming soon
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>