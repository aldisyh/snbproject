<!DOCTYPE html>
<html>
	<head>

		<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Airdrops Media</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Airdrops Media">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="https://airdrops.media/img/core-img/safenebula-logo2.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="https://airdrops.media/img/core-img/safenebula-logo2.ico">


		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1.0, shrink-to-fit=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700" rel="stylesheet" type="text/css">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('landing/vendor/bootstrap/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/vendor/fontawesome-free/css/all.min.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/vendor/animate/animate.min.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/vendor/simple-line-icons/css/simple-line-icons.min.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/vendor/owl.carousel/assets/owl.carousel.min.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/vendor/owl.carousel/assets/owl.theme.default.min.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/vendor/magnific-popup/magnific-popup.min.css')}}">

		<!-- Theme CSS -->
		<link rel="stylesheet" href="{{ asset('landing/css/theme.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/css/theme-elements.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/css/theme-blog.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/css/theme-shop.css')}}">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href="{{ asset('landing/vendor/rs-plugin/css/settings.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/vendor/rs-plugin/css/layers.css')}}">
		<link rel="stylesheet" href="{{ asset('landing/vendor/rs-plugin/css/navigation.css')}}">
		
		<!-- Demo CSS -->
		<link rel="stylesheet" href="{{ asset('landing/css/demos/demo-finance.css')}}">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="{{ asset('landing/css/skins/skin-finance.css')}}"> 

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" href="{{ asset('landing/css/custom.css')}}">

		<!-- Head Libs -->
		<script src="{{ asset('landing/vendor/modernizr/modernizr.min.js')}}"></script>
		<style>
			.nav-brand{
				font-size: 25px;
				font-family: 'Poppins', sans-serif;
				margin-left: 10px;
			}
			.contact-us-footer
			{
				margin-bottom: 0;
				font-size: 14px;
			}

			.footer-social-info a i{
				color: #fff;
				font-size: 14px;
				margin-right: 15px;
			}
		</style>
	</head>
	<body>

		<div class="body">
			 @include('landing.layouts.navbar')
			
			<div role="main" class="main">
				@yield('content')
			</div>

			<footer id="footer" class="custom-footer bg-color-quaternary m-0" >
				<div class="container py-3">
					<div class="row text-center text-lg-left pt-5 pb-3">
						<div class="col-md-4 mb-4 mb-lg-0">
							<a href="#" class="text-decoration-none">
								<img src="https://airdrops.media/img/core-img/safenebula-logo2.png" width="45" class="img-fluid" alt />
								&nbsp;<span style="color:#fff; font-size:20px">Airdrops Media</span>
							</a><br>
							<p class="custom-text-color-1">Airdrops Media is a media platform that provides information about cryptocurrency to the public based on blockchain.</p>
							<!-- Social Icon -->
                            {{--<div class="footer-social-info fadeInUp" data-wow-delay="0.4s">
                                <a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a>
                                <a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a>
                                <a href="https://youtube.com/channel/UCIBTROsm7qC3Whww0JELnLw"><i class="fab fa-youtube" aria-hidden="true"></i></a>
                                <a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a>
                                <a href="#"><i class="fab fa-telegram" aria-hidden="true"></i></a>
                                <a href="#"><i class="fas fa-envelope-open" aria-hidden="true"></i></a>
                            </div> --}}
						</div>
						<div class="col-md-5 mb-48 mb-lg-0">
							&nbsp;
						</div>

						{{-- <div class="col-md-2 mb-4 mb-lg-0">
							<h5 class="text-color-light font-weight-bold mb-1">Useful Links</h5>
							<ul>
								<li>
									<a class="custom-text-color-1" href="" target="_blank" title="Call Us">
										800 123 4567
									</a>
								</li>
								<li>
									<a class="custom-text-color-1" href="" title="Contact Us">
										Contact Us
									</a>
								</li>
							</ul>
						</div>

						<div class="col-md-2 mb-4 mb-lg-0">
							<h5 class="text-color-light font-weight-bold mb-1">Quick link</h5>
							<ul>
								<li>
									<a class="custom-text-color-1" href="" title="About Us">
										Airdrops
									</a>
								</li>
								<li>
									<a class="custom-text-color-1" href="" title="Team">
										ICO
									</a>
								</li>
								<li>
									<a class="custom-text-color-1" href="" title="Team">
										AMA
									</a>
								</li>
								<li>
									<a class="custom-text-color-1" href="" title="Team">
										INCUBATOR
									</a>
								</li>
								<li>
									<a class="custom-text-color-1" href="" title="Team">
										RANK
									</a>
								</li>
								<li>
									<a class="custom-text-color-1" href="" title="Team">
										FORUM
									</a>
								</li>
							</ul>
						</div> --}}
						<div class="col-md-3 mb-4 mb-lg-0">
							<h5 class="text-color-light font-weight-bold mb-1">Contact Us</h5>
							
							<p class="contact-us-footer">support@airdrops.media</p>
							{{-- <ul>
								<li>
									<a class="custom-text-color-1" href="#" title="Privacy policy">
										Privacy policy
									</a>
								</li>
								<li>
									<a class="custom-text-color-1" href="#" title="Terms of Use">
										Terms of Use
									</a>
								</li>
							</ul> --}}
							<div class="footer-social-info fadeInUp" data-wow-delay="0.4s">
		                                	<a href="#"><i class="fab fa-facebook" aria-hidden="true"></i></a>
			                                <a href="#"><i class="fab fa-twitter" aria-hidden="true"></i></a>
			                                <a href="https://youtube.com/channel/UCIBTROsm7qC3Whww0JELnLw"><i class="fab fa-youtube" aria-hidden="true"></i></a>
			                                <a href="#"><i class="fab fa-instagram" aria-hidden="true"></i></a>
			                                <a href="#"><i class="fab fa-telegram" aria-hidden="true"></i></a>
			                            	</div> 
						</div>
					</div>
				</div>
				<div class="footer-copyright bg-color-quaternary m-0 pt-3 pb-3">
					<div class="container">
						<div class="row">
							<div class="col-lg-12 text-center pt-3">
								<p class="custom-text-color-1">airdropsmedia.com 2021 © All rights reserved. </p>
							</div>
						</div>
					</div>
				</div>
			</footer>
		</div>

		<!-- Vendor -->
		<script src="{{ asset('landing/vendor/jquery/jquery.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/jquery.appear/jquery.appear.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/jquery.easing/jquery.easing.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/jquery.cookie/jquery.cookie.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/popper/umd/popper.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/common/common.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/jquery.validation/jquery.validate.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/jquery.easy-pie-chart/jquery.easypiechart.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/isotope/jquery.isotope.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/vide/jquery.vide.min.js')}}"></script>
		<script src="{{ asset('landing/vendor/vivus/vivus.min.js')}}"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{ asset('landing/js/theme.js')}}"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="{{ asset('landing/vendor/rs-plugin/js/jquery.themepunch.tools.min.j')}}s"></script>
		<script src="{{ asset('landing/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="{{ asset('landing/js/views/view.contact.js')}}"></script>

		<!-- Demo -->
		<script src="{{ asset('landing/js/demos/demo-finance.js')}}"></script>
		
		<!-- Theme Custom -->
		<script src="{{ asset('landing/js/custom.js')}}"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{ asset('landing/js/theme.init.js')}}"></script>



	</body>
</html>
