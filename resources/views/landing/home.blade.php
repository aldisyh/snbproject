@extends('landing.layouts.index')
@section('content')

<section class="section section-no-border custom-section-spacement-1 bg-color-light-scale-1 m-0">
    <div class="row justify-content-center">
        <div class="col-sm-10">
            <div class="owl-carousel owl-theme stage-margin nav-style-1 " data-plugin-options="{'items': 4, 'margin': 10, 'loop': true, 'nav': true, 'dots': false, 'stagePadding': 40}">
                <div>
                    <img alt="" class="img-fluid rounded" src="{{ asset('landing/img/banner-1.jpeg')}}">
                </div>
                <div>
                    <img alt="" class="img-fluid rounded" src="{{ asset('landing/img/banner-1.jpeg')}}">
                </div>
                <div>
                    <img alt="" class="img-fluid rounded" src="{{ asset('landing/img/banner-1.jpeg')}}">
                </div>
                <div>
                    <img alt="" class="img-fluid rounded" src="{{ asset('landing/img/banner-1.jpeg')}}">
                </div>
                <div>
                    <img alt="" class="img-fluid rounded" src="{{ asset('landing/img/banner-1.jpeg')}}">
                </div>
                <div>
                    <img alt="" class="img-fluid rounded" src="{{ asset('landing/img/banner-1.jpeg')}}">
                </div>
                <div>
                    <img alt="" class="img-fluid rounded" src="{{ asset('landing/img/banner-1.jpeg')}}">
                </div>
                <div>
                    <img alt="" class="img-fluid rounded" src="{{ asset('landing/img/banner-1.jpeg')}}">
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row text-center mb-3 mt-5">
            <div class="col">
                <h2 class="text-uppercase text-color-dark font-weight-bold">The Latest Airdrops</h2>
            </div>
        </div>
        <div class="row">
        @php
          $now_date = strtotime($now_date);
        @endphp
        @foreach($project_all as $key => $project)

        @php
          $participant = App\Model\Participants::where('airdrop_id',$project->id)->first();
          $start_date = strtotime($project->airdrop[0]->airdrop_starts);
          $end_date = strtotime($project->airdrop[0]->airdrop_ends);

          $day_left = date('d', $end_date - $now_date) - 1;

        @endphp
            <div class="col-sm-6 col-lg-3 mt-3 border-radius-2">
            <div class="ribbon ribbon-top-left">
                @if($project->is_status == 1 && $day_left != 0 ) <span class="ribbon-danger"><b>Hot</b></span>
                @elseif($project->is_status == 2 && $day_left != 0 ) <span class="ribbon-success"><b>New</b></span>
                @elseif($project->is_status == 3 || $day_left == 0 ) <span class="ribbon-ended"><b>Ended</b></span>
                @endif
                </span></div>
                <div class="card border-radius-1 bg-color-light border-0 box-shadow-2">
                    <div class="card-content">
                        <div class="card-body">
                        <div class="row justify-content-center">
                            <div class="col-6">
                                <img class="img-fluid custom-rounded-image" src="{{asset('storage/images/' . $project->logo)}}">
                            </div>
                            <div class="col-6 mt-2">
                                <span class="thumb-info-caption bg-color-light">
                                @if ( $project->is_status == 3 ||  $day_left == 0 )
                                    <a href="#">
                                @else
                                  <a href="{{url('airdrop')}}/{{$project->slug}}">
                                @endif
                                        <h5 class="card-title">{{$project->title}}</h5>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <hr>

                        <center><b class="mb-2">Coin / Token</b></center>
                        <div class="row justify-content-center">
                          <div class="col">
                          Rewards
                              <h6 class="card-title">{{!empty($participant->rewards) ? $participant->rewards : 0 }}</h6>
                          </div>
                          <div class="col">
                          Referral
                              <h6 class="card-title">{{!empty($participant->referral_bonus) ? $participant->referral_bonus : 0}}</h6>
                          </div>
                        </div>

                        <hr>
                        <center><b class="mb-2">Estimated Value</b></center>
                        <div class="row justify-content-center">
                          <div class="col">
                          Rewards
                              <h6 class="card-title">$ {{ !empty($participant->value_rewards) ? $participant->value_rewards : 0 }}</h6>
                          </div>
                          <div class="col">
                          Referral
                              <h6 class="card-title">$ {{ !empty($participant->value_referral) ? $participant->value_referral : 0 }}</h6>
                          </div>
                        </div>
                        <hr>
                        <div class="row align-items-end">
                            <div class="col">
                                <span class="social-icons-facebook"> 
                                    <a href="{{ url($project->twitter)}}" target="_blank" title="Twitter">
                                        <i class="fab fa-twitter" style="font-size:24px"></i>
                                    </a>
                                </span>
                            </div>
                            <div class="col">
                                <span class="social-icons-telegram">
                                    <a href="{{ url($project->telegram)}}" target="_blank" title="Telegram">
                                        <i class="fab fa-telegram" style="font-size:24px"></i>
                                    </a>
                                </span>
                            </div>
                            <div class="col"><span class="social-icons-telegram">
                                    <a href="{{ url($project->airdrop[0]->airdrop_link)}}" target="_blank" title="Website">
                                        <i class="fas fa-globe" style="font-size:24px"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                        <hr>
                        <div class="row justify-content-around">
                            <div class="col">
                                <a href="#">
                                @if ( $project->is_status == 3 ||  $day_left == 0 )
                                  <button type="button" class="btn-coin btn-coin-ended btn-coint-view">Ended</button>
                                @else
                                  <a href="{{url('airdrop')}}/{{$project->slug}}">
                                    <button type="button" class="btn-coin btn-coin-success btn-coint-view">Detail </button>
                                  </a>
                                @endif
                                </a>
                            </div>
                            
                            
                            {{-- 
                            starts : {{date('d F Y', strtotime($project->airdrop[0]->airdrop_starts))}}<br>
                            now_date : {{date('d F Y', $now_date)}}<br>
                            ends : {{date('d F Y', strtotime($project->airdrop[0]->airdrop_ends))}} <br>

                            {{(int)$end_date}} - {{$now_date}} --}}
                            <div class="col">
                                <a href="#">
                                  <button type="button" class="btn-coin btn-coin-ended">{{ $day_left }} Days Left</button>
                                </a>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
            </div>
        @endforeach

        </div>
        <div class="row">
          <div class="col-sm-10 ml-5 mt-2">
            <ul class="pagination custom-pagination-style-1 float-right">
             {{ $project_all->render() }}
              {{-- <li class="page-item"><a class="page-link text-color-dark" href="#">«</a></li>
              <li class="page-item active"><a class="page-link text-color-dark" href="#">1</a></li>
              <li class="page-item"><a class="page-link text-color-dark" href="#">2</a></li>
              <li class="page-item"><a class="page-link text-color-dark" href="#">3</a></li>
              <li class="page-item"><a class="page-link text-color-dark" href="#">»</a></li> --}}
            </ul>
          </div>
        </div>
    </div>

</section>

@endsection