@extends('landing.layouts.index')
@section('content')


<section class="page-header page-header-modern bg-color-light-scale-1 page-header-md">
    <div class="container">
        <div class="row">
            <div class="col-md-8 order-2 order-md-1 align-self-center p-static">
                <h1 class="text-dark"><strong>{{$project_all->title}}</strong></h1>
            </div>

            <div class="col-md-4 order-1 order-md-2 align-self-center">
                <ul class="breadcrumb d-block text-md-right">
                    <li><a href="#">{{$project_all->title}}</a></li>
                    <li class="active">Detil Information</li>
                </ul>
            </div>
        </div>
        
    </div>
    </section>

    <section class="section section-no-border bg-color-light m-0 pb-0">
    <div class="container">
        <div class="row">
            <article class="custom-post-style-1">
                <div class="col-lg-10 col-sm-10">
                
                    <img src="{{asset('storage/images/' . $project_all->logo)}}" width="300" class="img-fluid mb-4" alt="">
                    
                    <ul class="custom-list-style-1 mt-3">
                        <li>
                            <a href="#">
                                <h6 class="text-color-dark font-weight-semibold mb-3">
                                    Starts : {{ date('F d, Y', strtotime($detail->airdrop_starts)) }}
                                </h6>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <h6 class="text-color-dark font-weight-semibold mb-3">
                                    Ends : {{date('F d, Y', strtotime($detail->airdrop_ends))}}
                                </h6>
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <h6 class="text-color-dark font-weight-semibold mb-3">
                                     Distribution : {{date('F d, Y', strtotime($detail->airdrop_distribution))}}
                                </h6>
                            </a>
                        </li>
                        {{-- <li>
                            <a href="#">
                                <h6 class="text-color-dark font-weight-semibold mb-3">
                                    Rewards: {{ !empty($participant->rewards) ? $participant->rewards : 0 }}</h6>
                            </a>
                        </li> --}}

                        <li>
                            <a href="#">
                                <h6 class="text-color-dark font-weight-semibold mb-3">
                                    Max Participants : {{ !empty($participant->maximum_participant) ? $participant->maximum_participant : 0 }}
                                </h6>
                            </a>
                        </li>

                        {{-- <li>
                            <a href="#">
                                <h6 class="text-color-dark font-weight-semibold mb-3">
                                    Referral Bonus : {{ !empty($participant->referral_bonus) ? $participant->referral_bonus : 0}}
                                </h6>
                            </a>
                        </li> --}}
                    </ul>
                    <br>
                    <a href="{{url($detail->airdrop_link)}}" class="btn btn-success">Join Airdrop</a>
                </div>
            </article>
            <div class="col-lg-6 col-sm-6">
                <h2 class="font-weight-semibold text-color-dark mb-3">{{$project_all->title}}</h2>
                    <p class="custom-font-style-1 font-weight-semibold">{{$project_all->title}} 
                    is 
                    @if($project_all->is_status==0) <b>New</b>.
                    @elseif($project_all->is_status==1) <b>Featured</b>.
                    @elseif($project_all->is_status==2) <b>Ended</b>.
                    @endif
                    </p>


                    <div class="row ml-3 mb-4">
                        <div class="row align-items-end">
                            <div class="col">
                                <span class="social-icons-facebook"> 
                                    <a href="{{ url($project_all->twitter)}}" target="_blank" title="Twitter">
                                        <i class="fab fa-twitter" style="font-size:24px"></i>
                                    </a>
                                </span>
                            </div>
                            <div class="col">
                                <span class="social-icons-telegram">
                                    <a href="{{ url($project_all->telegram)}}" target="_blank" title="Telegram">
                                        <i class="fab fa-telegram" style="font-size:24px"></i>
                                    </a>
                                </span>
                            </div>
                            <div class="col"><span class="social-icons-telegram">
                                    <a href="{{ url($project_all->airdrop[0]->airdrop_link)}}" target="_blank" title="Website">
                                        <i class="fas fa-globe" style="font-size:24px"></i>
                                    </a>
                                </span>
                            </div>
                        </div>
                    </div>

                    
                    <p class="text-2 custom-line-height-1">
                    {!!$project_all->description!!}
                    </p>	
                    <hr>
                    {{-- <h2 class="font-weight-semibold text-color-dark mb-2">Estimated value</h2>
                    <p class="elementor-text-editor elementor-clearfix">$56 Prize pool</p>
                    <hr>
                    <h2 class="font-weight-semibold text-color-dark mb-2">Step By Step Guide</h2>
                    <p class="elementor-text-editor elementor-clearfix">
                            1. Go to the airdrop page. </br>

                            2. Join the Telegram group and channel. </br>

                            3. Follow  Twitter and retweet pinned post.</br>

                            4. Like and retweet the airdrop tweet. </br>

                            5. Then simply click on the claim button on the airdrop page.</br>

                            6. Submit Details and  Submit your BEP20 BNB Address.</br>
                    </p> --}}
            </div>

        </div>
    </div>
    </section>

@endsection