@extends('member.layouts.index')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/bundles/bootstrap-social/bootstrap-social.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/summernote/summernote-bs4.css') }}">
@endsection

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Profile</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('member.dashboard') }}">Dashboard</a></div>
            <div class="breadcrumb-item">Profile</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row mt-sm-4">
            <div class="col-12 col-md-12 col-lg-4">
                <div class="card author-box">
                    <div class="card-body">
                        <div class="author-box-center">
                            <img alt="image" src="{{ asset('assets_member/img/core-img/safenebula-logo2 (1).png') }}" class="rounded-circle author-box-picture">
                            <div class="clearfix"></div>
                            <div class="author-box-name">
                                <a href="#">{{ !empty($user) ? $user->name : '' }}</a>
                            </div>
                            <div class="author-box-job">Member</div>
                        </div>
                        <div class="text-center">
                            <!-- <div class="author-box-description">
                                <p>
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur voluptatum alias molestias
                                    minus quod dignissimos.
                                </p>
                            </div> -->
                            <div class="mb-2 mt-3">
                                <div class="text-small font-monospace-bold">Share Profile {{ !empty($user) ? $user->name : '' }} On</div>
                            </div>
                            <a href="https://www.facebook.com/sharer/sharer.php?u=abbabill.com&display=popup" class="btn btn-social-icon mr-1 btn-facebook">
                                <i class="fab fa-facebook-f"></i>
                            </a>
                            <!-- <a href="#" class="btn btn-social-icon mr-1 btn-twitter">
                                <i class="fab fa-twitter"></i>
                            </a>
                            <a href="#" class="btn btn-social-icon mr-1 btn-github">
                                <i class="fab fa-github"></i>
                            </a>
                            <a href="#" class="btn btn-social-icon mr-1 btn-instagram">
                                <i class="fab fa-instagram"></i>
                            </a> -->
                            <div class="w-100 d-sm-none"></div>
                        </div>
                    </div>
                </div>
                <!-- <div class="card">
                    <div class="card-header">
                        <h4>Personal Details</h4>
                    </div>
                    <div class="card-body">
                        <div class="py-4">
                            <p class="clearfix">
                                <span class="float-left">
                                    Birthday
                                </span>
                                <span class="float-right text-muted">
                                    30-05-1998
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Phone
                                </span>
                                <span class="float-right text-muted">
                                    (0123)123456789
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Mail
                                </span>
                                <span class="float-right text-muted">
                                    test@example.com
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Facebook
                                </span>
                                <span class="float-right text-muted">
                                    <a href="#">John Deo</a>
                                </span>
                            </p>
                            <p class="clearfix">
                                <span class="float-left">
                                    Twitter
                                </span>
                                <span class="float-right text-muted">
                                    <a href="#">@johndeo</a>
                                </span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h4>Skills</h4>
                    </div>
                    <div class="card-body">
                        <ul class="list-unstyled user-progress list-unstyled-border list-unstyled-noborder">
                            <li class="media">
                                <div class="media-body">
                                    <div class="media-title">Java</div>
                                </div>
                                <div class="media-progressbar p-t-10">
                                    <div class="progress" data-height="6">
                                        <div class="progress-bar bg-primary" data-width="70%"></div>
                                    </div>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <div class="media-title">Web Design</div>
                                </div>
                                <div class="media-progressbar p-t-10">
                                    <div class="progress" data-height="6">
                                        <div class="progress-bar bg-warning" data-width="80%"></div>
                                    </div>
                                </div>
                            </li>
                            <li class="media">
                                <div class="media-body">
                                    <div class="media-title">Photoshop</div>
                                </div>
                                <div class="media-progressbar p-t-10">
                                    <div class="progress" data-height="6">
                                        <div class="progress-bar bg-green" data-width="48%"></div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </div> -->
            </div>
            <div class="col-12 col-md-12 col-lg-8">
                <div class="card">
                    <div class="padding-20">
                        <ul class="nav nav-tabs" id="myTab2" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab2" data-toggle="tab" href="#about" role="tab" aria-selected="true">About</a>
                            </li>
                            <!-- <li class="nav-item">
                                <a class="nav-link" id="profile-tab2" data-toggle="tab" href="#settings" role="tab" aria-selected="false">Setting</a>
                            </li> -->
                        </ul>
                        <div class="tab-content tab-bordered" id="myTab3Content">
                            <div class="tab-pane fade show active" id="about" role="tabpanel" aria-labelledby="home-tab2">
                                <div class="row">
                                    <div class="col-md-3 col-6 b-r">
                                        <strong>Full Name</strong>
                                        <br>
                                        <p style="font-family:monospace;">{{ !empty($user) ? $user->name : '' }}</p>
                                    </div>
                                    <div class="col-md-3 col-6 b-r">
                                        <strong>Location</strong>
                                        <br>
                                        <p style="font-family:monospace;">{{ !empty($user->country) ? $user->country->name : '' }}</p>
                                    </div>
                                    <div class="col-md-3 col-6 b-r">
                                        <strong>Email</strong>
                                        <br>
                                        <p style="font-family:monospace;">{{ !empty($user) ? $user->email : '' }}</p>
                                    </div>
                                    <div class="col-md-3 col-6">
                                        <strong>Action</strong>
                                        <br>
                                        <button type=" button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#gantipassword" data-whatever="{{ $user->id }}" data-name="{{ $user->name }}" data-email="{{ $user->email }}" data-password="">Change Password</button>
                                    </div>
                                </div>
                                <!-- <p class="m-t-30">Completed my graduation in Arts from the well known and
                                    renowned institution
                                    of India – SARDAR PATEL ARTS COLLEGE, BARODA in 2000-01, which was
                                    affiliated
                                    to M.S. University. I ranker in University exams from the same
                                    university
                                    from 1996-01.</p>
                                <p>Worked as Professor and Head of the department at Sarda Collage, Rajkot,
                                    Gujarat
                                    from 2003-2015 </p>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting
                                    industry. Lorem
                                    Ipsum has been the industry's standard dummy text ever since the 1500s,
                                    when
                                    an unknown printer took a galley of type and scrambled it to make a
                                    type
                                    specimen book. It has survived not only five centuries, but also the
                                    leap
                                    into electronic typesetting, remaining essentially unchanged.</p>
                                <div class="section-title">Education</div>
                                <ul>
                                    <li>B.A.,Gujarat University, Ahmedabad,India.</li>
                                    <li>M.A.,Gujarat University, Ahmedabad, India.</li>
                                    <li>P.H.D., Shaurashtra University, Rajkot</li>
                                </ul>
                                <div class="section-title">Experience</div>
                                <ul>
                                    <li>One year experience as Jr. Professor from April-2009 to march-2010
                                        at B.
                                        J. Arts College, Ahmedabad.</li>
                                    <li>Three year experience as Jr. Professor at V.S. Arts &amp; Commerse
                                        Collage
                                        from April - 2008 to April - 2011.</li>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                    <li>Lorem Ipsum is simply dummy text of the printing and typesetting
                                        industry.
                                    </li>
                                </ul> -->
                            </div>
                            <div class="tab-pane fade" id="settings" role="tabpanel" aria-labelledby="profile-tab2">
                                <form method="post" class="needs-validation">
                                    <div class="card-header">
                                        <h4>Edit Profile</h4>
                                    </div>
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="form-group col-md-6 col-12">
                                                <label>First Name</label>
                                                <input type="text" class="form-control" value="John">
                                                <div class="invalid-feedback">
                                                    Please fill in the first name
                                                </div>
                                            </div>
                                            <div class="form-group col-md-6 col-12">
                                                <label>Last Name</label>
                                                <input type="text" class="form-control" value="Deo">
                                                <div class="invalid-feedback">
                                                    Please fill in the last name
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-md-7 col-12">
                                                <label>Email</label>
                                                <input type="email" class="form-control" value="test@example.com">
                                                <div class="invalid-feedback">
                                                    Please fill in the email
                                                </div>
                                            </div>
                                            <div class="form-group col-md-5 col-12">
                                                <label>Phone</label>
                                                <input type="tel" class="form-control" value="">
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group col-12">
                                                <label>Bio</label>
                                                <textarea class="form-control summernote-simple">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Pariatur voluptatum alias molestias minus quod dignissimos.</textarea>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="form-group mb-0 col-12">
                                                <div class="custom-control custom-checkbox">
                                                    <input type="checkbox" name="remember" class="custom-control-input" id="newsletter">
                                                    <label class="custom-control-label" for="newsletter">Subscribe to newsletter</label>
                                                    <div class="text-muted form-text">
                                                        You will get new information about products, offers and promotions
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-footer text-right">
                                        <button class="btn btn-primary">Save Changes</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<div class="modal fade" id="gantipassword" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form action="{{ route('change.password') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Change Password</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group d-none hide">
                        <label for="recipient-name" class="col-form-label">id:</label>
                        <input type="text" name="id" class="form-control" id="recipient-name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Name : </label>
                        <input type="text" name="name" class="form-control" placeholder="" id="name">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Email : </label>
                        <input type="text" name="email" class="form-control" placeholder="" id="email">
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="col-form-label">Password : </label>
                        <input type="password" name="password" placeholder="" class="form-control" id="password">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@section('js')
@if(Session::has('success'))
<script>
    swal("success", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
@if(Session::has('warning'))
<script>
    swal("warning", "{!!Session::get('warning')!!}", "warning", {
        button: "OK",
    });
</script>
@endif

<script src="{{ asset('assets/bundles/summernote/summernote-bs4.js') }}"></script>

<script>
    $('#gantipassword').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget) // Button that triggered the modal
        var recipient = button.data('whatever') // Extract info from data-* attributes
        var name = button.data('name') // Extract info from data-* attributes
        var email = button.data('email') // Extract info from data-* attributes
        var password = button.data('password') // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('New Password')
        modal.find('#recipient-name').val(recipient)
        modal.find('#name').val(name)
        modal.find('#email').val(email)
        modal.find('#password').val(password)
    })
</script>
@endsection