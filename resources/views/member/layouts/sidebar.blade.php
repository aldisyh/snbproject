<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="#">
            <img alt="image" src="{{ asset('assets_member/img/core-img/safenebula-logo2 (1).png') }}" class="header-logo" />
            <span class="logo-name">Airdrops</span>
        </a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Main</li>
        <li class="dropdown">
            <a href="{{ route('member.dashboard') }}" class="nav-link">
                <i class="fas fa-home"></i><span>Dashboard</span>
            </a>
        </li>
        <li class="dropdown">
            <a href="{{ route('profile.index') }}" class="nav-link">
                <i class="fas fa-user"></i><span>Profile</span>
            </a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fab fa-product-hunt"></i><span>Airdrops</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('postingan.index') }}">List Post</a></li>
                <li><a class="nav-link" href="{{ route('postingan.form') }}">Create Post</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link" data-toggle="modal" data-target="#no_link">
                <i class="fas fa-circle"></i><span>ICO</span>
            </a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link" data-toggle="modal" data-target="#no_link">
                <i class="fas fa-circle"></i><span>AMA</span>
            </a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link" data-toggle="modal" data-target="#no_link">
                <i class="fas fa-circle"></i><span>Incubator</span>
            </a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link" data-toggle="modal" data-target="#no_link">
                <i class="fas fa-circle"></i><span>Rank</span>
            </a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link" data-toggle="modal" data-target="#no_link">
                <i class="fas fa-circle"></i><span>Forum</span>
            </a>
        </li>
        <li class="dropdown">
            <a href="{{ route('logout') }}" class="nav-link" data-toggle="modal" data-target="#no_link">
                <i class="fas fa-sign-out-alt"></i><span>Logout</span>
            </a>
        </li>
        
    </ul>
</aside>