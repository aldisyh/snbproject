<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>Airdrops Media - Register Account</title>

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Airdrops Media">
    <meta name="author" content="okler.net">
    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/css/app.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/bundles/jquery-selectric/selectric.css') }}">
    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-sweetalert/1.0.1/sweetalert.css">
    <!-- Custom style CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/custom.css') }}">
    
    <!-- Favicon -->
    <link rel="shortcut icon" href="https://airdrops.media/img/core-img/safenebula-logo2.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="https://airdrops.media/img/core-img/safenebula-logo2.ico">

</head>

<body>
    <div class="loader"></div>
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h4>Register</h4>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('post.register') }}" class="needs-validation" novalidate="">
                                    @csrf
                                    <div class="row">
                                        <div class="form-group col-md-12 col-xs-12">
                                            <label for="frist_name">Full Name</label>
                                            <input id="frist_name" type="text" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" name="name" value="{{ old('name') }}" placeholder="Fullname" required autofocus>
                                            @if($errors->has('name'))
                                            <div class="invalid-feeedback">
                                                {{$errors->first('name') }}
                                            </div>
                                            @endif
                                        </div>

                                        <div class="form-group col-md-12 col-xs-12">
                                            <label for="email">Email</label>
                                            <input id="email" type="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Email" required>
                                            @if($errors->has('email'))
                                            <div class="invalid-feeedback">
                                                {{$errors->first('email') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12 col-xs-12">
                                            <label for="password" class="d-block">Password</label>
                                            <input id="password" type="password" class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" name="password" placeholder="Password" required>
                                            @if($errors->has('password'))
                                            <div class="invalid-feeedback">
                                                {{$errors->first('password') }}
                                            </div>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-12 col-xs-12">
                                            <label for="password" class="d-block">Password Confirmation</label>
                                            <input class="form-control {{ $errors->has('password_confirmation') ? 'is-invalid' : '' }}" type="password" name="password_confirmation" id="inputpassword_confirmation" placeholder="Password Confirmation" required>
                                            @if($errors->has('password_confirmation'))
                                            <div class="invalid-feeedback">
                                                {{$errors->first('password_confirmation') }}
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-12 col-xs-12">
                                            <label>Country</label>
                                            <select class="form-control" name="country_id">
                                                <option value="">== Select Country ==</option>
                                                @foreach($countrys as $key => $country)
                                                    <option value="{{$country->id}}">{{$country->name}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <!-- <div class="form-group">
                                        <div class="custom-control custom-checkbox">
                                            <input type="checkbox" name="agree" class="custom-control-input" id="agree">
                                            <label class="custom-control-label" for="agree">I agree with the terms and conditions</label>
                                        </div>
                                    </div> -->
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                                            Register
                                        </button>
                                    </div>
                                </form>
                            </div>
                            <div class="mb-4 text-muted text-center">
                                Already Registered? <a href="{{ route('member.form') }}">Login</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <!-- General JS Scripts -->
    <script src="{{ asset('assets/js/app.min.js') }}"></script>
    <!-- JS Libraies -->
    <script src="{{ asset('assets/bundles/jquery-pwstrength/jquery.pwstrength.min.js') }}"></script>
    <script src="{{ asset('assets/bundles/jquery-selectric/jquery.selectric.min.js') }}"></script>
    <!-- Page Specific JS File -->
    <script src="{{ asset('assets/js/page/auth-register.js') }}"></script>
    <!-- Template JS File -->
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <!-- Custom JS File -->
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

    @if(Session::has('success'))
    <script>
        swal("Success", "{!!Session::get('success')!!}", "success", {
            button: "OK",
        });
    </script>
    @endif

</body>

</html>