@extends('member.layouts.index')

@section('content')
<section class="section">
    <div class="section-header">
    <div class="section-header-back">
            <a href="{{ route('postingan.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Detail Post</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('member.dashboard') }}">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="{{ route('postingan.index') }}">Post</a></div>
            <div class="breadcrumb-item">Detail Post</div>
        </div>
    </div>
    <div class="section-body">
        <div class="card card-warning">
            <div class="card-body ">
                <div class="row">
                    <div class="col-12 col-md-6 col-lg-12">
                        <div class="section-title" style="font-family:monospace;" size="30px" color="#FF7A59">{{ !empty($post) ? $post->title : '' }}</div>
                        <hr>
                        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner">
                                <div class="carousel-item active">
                                    <img class="d-block img-thumbnail img-rounded" src="{{ asset('storage/images/' . $post->logo) }}" width="300" alt="{{ $post->logo }}">
                                </div>
                            </div>
                        </div>
                        {{-- <p class="text-muted">{{ !empty($post) ? $post->created_at : '' }}</p> --}}
                    </div>
                </div>
                <br>
                <br>
                <div class="col-12 col-md-6 col-lg-12">
                    {{-- <div class="section-title" size="20px" color="#FF7A59">{{ !empty($post) ? $post->title : '' }}
                </div> --}}
                <div class="row">
                    <div class="col-md-4 col-6 b-r">
                        <strong style="font-family:monospace;">Website</strong>
                        <br>
                        <p style="font-family:courier;">{{ !empty($post) ? $post->website : '' }}</p>
                    </div>
                    <div class="col-md-4 col-6 b-r">
                        <strong style="font-family:monospace;">Telegram</strong>
                        <br>
                        <p style="font-family:courier;">{{ !empty($post) ? $post->telegram : '' }}</p>
                    </div>
                    <div class="col-md-4 col-6 b-r">
                        <strong style="font-family:monospace;">Twitter</strong>
                        <br>
                        <p style="font-family:courier;">{{ !empty($post) ? $post->twitter : '' }}</p>
                    </div>
                </div>
                <hr>
                <div class="section-title" style="font-family:monospace;">Description</div>
                <p class="m-t-30">{!! !empty($post) ? $post->description : '' !!}</p>
                <br>
                <div class="col-12 col-md-6 col-lg-12">
                    <div class="row">
                        <div class="col-md-6 col-6 b-r">
                            <strong style="font-family:monospace;">Airdrop Links</strong>
                            <br>
                            <p style="font-family:courier;">{{ !empty($post['airdrop']) ? $post['airdrop']->airdrop_link : '' }}</p>
                        </div>
                        <div class="col-md-6 col-6 b-r">
                            <strong style="font-family:monospace;">Airdrop Start</strong>
                            <br>
                            <p style="font-family:courier;">{{ !empty($post['airdrop']) ? $post['airdrop']->airdrop_starts : '' }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 col-6 b-r">
                            <strong style="font-family:monospace;">Airdrop End</strong>
                            <br>
                            <p style="font-family:courier;">{{ !empty($post['airdrop']) ? $post['airdrop']->airdrop_ends : '' }}</p>
                        </div>
                        <div class="col-md-6 col-6 b-r">
                            <strong style="font-family:monospace;">Airdrop Distribution</strong>
                            <br>
                            <p style="font-family:courier;">{{ !empty($post['airdrop']) ? $post['airdrop']->airdrop_distribution : '' }}</p>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="col-12 col-md-6 col-lg-12">
                    <div class="row">
                        <div class="col-md-6 col-6 b-r">
                            <strong style="font-family:monospace;">Reward</strong>
                            <br>
                            <p style="font-family:courier;">{{ !empty($post['participant']) ? $post['participant']->rewards : '' }}</p>
                        </div>
                        <div class="col-md-6 col-6 b-r">
                            <strong style="font-family:monospace;">Referall Bonus</strong>
                            <br>
                            <p style="font-family:courier;">{{ !empty($post['participant']) ? $post['participant']->referral_bonus : '' }}</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-6 b-r">
                            <strong style="font-family:monospace;">Maximum Participant</strong>
                            <br>
                            <p style="font-family:courier;">{{ !empty($post['participant']) ? $post['participant']->maximum_participant : '' }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
</section>
@endsection

@section('js')
@if(Session::has('success'))
<script>
    swal("Success", "{!!Session::get('success')!!}", "success", {
        button: "OK",
    });
</script>
@endif
@endsection