@extends('member.layouts.index')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Your Dashboard</h1>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <i class="fas fa-cart-plus card-icon col-red"></i>
                <div class="card-wrap">
                    <div class="padding-20">
                        <div class="text-right">
                            <h3 class="font-light mb-0">
                                <i class="ti-arrow-up text-success"></i> {{$project_all}}
                            </h3>
                            <span class="text-muted">Your Post's</span>
                        </div>
                        {{-- <p class="mb-3 text-muted pull-left text-sm">
                            <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 10%</span>
                            <span class="text-nowrap"></span>
                        </p> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <i class="fas fa-hiking card-icon col-green"></i>
                <div class="card-wrap">
                    <div class="padding-20">
                        <div class="text-right">
                            <h3 class="font-light mb-0">
                                <i class="ti-arrow-up text-success"></i> {{$project_approve}}
                            </h3>
                            <span class="text-muted">Approve Post's</span>
                        </div>
                        {{-- <p class="mb-3 text-muted pull-left text-sm">
                            <span class="col-orange mr-2"><i class="fa fa-arrow-down"></i> 5%</span>
                            <span class="text-nowrap"></span>
                        </p> --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <i class="fas fa-chart-line card-icon col-orange"></i>
                <div class="card-wrap">
                    <div class="padding-20">
                        <div class="text-right">
                            <h3 class="font-light mb-0">
                                <i class="ti-arrow-up text-success"></i> {{$project_pending}}
                            </h3>
                            <span class="text-muted">Pending Post's</span>
                        </div>
                        {{-- <p class="mb-3 text-muted pull-left text-sm">
                            <span class="text-success mr-2"><i class="fa fa-arrow-up"></i> 24%</span>
                            <span class="text-nowrap">Since last month</span>
                        </p> --}}
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="col-lg-3 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <i class="fas fa-dollar-sign card-icon col-cyan"></i>
                <div class="card-wrap">
                    <div class="padding-20">
                        <div class="text-right">
                            <h3 class="font-light mb-0">
                                <i class="ti-arrow-up text-success"></i> $5,263
                            </h3>
                            <span class="text-muted">Todays Earning</span>
                        </div>
                        <p class="mb-3 text-muted pull-left text-sm">
                            <span class="col-orange mr-2"><i class="fa fa-arrow-down"></i> 7.5%</span>
                            <span class="text-nowrap">Since last month</span>
                        </p>
                    </div>
                </div>
            </div>
        </div> --}}
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <a href="{{ route('postingan.form') }}" class="btn btn-success btn-block"> Request Post</a>
        </div>
    </div>
</section>
@endsection