<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <a href="#">
            <img alt="image" src="{{ asset('assets_member/img/core-img/safenebula-logo2 (1).png') }}" class="header-logo" />
            <span class="logo-name">Airdrops</span>
        </a>
    </div>
    <ul class="sidebar-menu">
        <li class="menu-header">Main</li>
        <li class="dropdown">
            <a href="{{ route('admin.home') }}" class="nav-link">
                <i class="fas fa-home"></i><span>Dashboard</span>
            </a>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fab fa-product-hunt"></i><span>Post</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('post.index') }}">List Post</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="nav-link has-dropdown"><i class="fas fa-user"></i><span>User</span></a>
            <ul class="dropdown-menu">
                <li><a class="nav-link" href="{{ route('user.index') }}">List User</a></li>
            </ul>
        </li>
        <!-- <li class="dropdown">
            <a href="{{ route('logout') }}" class="nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="fas fa-power-off"></i><span>Logout</span>
            </a>
            <form id="logout-form" action="{{ route('logout') }}" class="d-none">
                @csrf
            </form>
        </li> -->
    </ul>
</aside>