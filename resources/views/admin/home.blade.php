@extends('admin.layouts.index')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Dashboard</h1>
    </div>
    <div class="row">
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <i class="fas fa-cart-plus card-icon col-red"></i>
                <div class="card-wrap">
                    <div class="padding-20">
                        <div class="text-right">
                            <h3 class="font-light mb-0">
                                <i class="ti-arrow-up text-success"></i> {{$project_all}}
                            </h3>
                            <span class="text-muted">All Post's</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <i class="fas fa-hiking card-icon col-green"></i>
                <div class="card-wrap">
                    <div class="padding-20">
                        <div class="text-right">
                            <h3 class="font-light mb-0">
                                <i class="ti-arrow-up text-success"></i> {{$project_approve}}
                            </h3>
                            <span class="text-muted">Approve Post's</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-6 col-sm-6 col-12">
            <div class="card card-statistic-1">
                <i class="fas fa-chart-line card-icon col-orange"></i>
                <div class="card-wrap">
                    <div class="padding-20">
                        <div class="text-right">
                            <h3 class="font-light mb-0">
                                <i class="ti-arrow-up text-success"></i> {{$project_pending}}
                            </h3>
                            <span class="text-muted">Pending Post's</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection