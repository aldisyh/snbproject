@extends('admin.layouts.index')

@section('css')
<link rel="stylesheet" href="{{ asset('assets/bundles/summernote/summernote-bs4.css') }}">
<link rel="stylesheet" href="{{ asset('assets/bundles/jquery-selectric/selectric.css') }}">
@endsection

@section('content')

<style>
.image-preview{
    background-image: url("{{asset('storage/images/' . $post->logo)}}");
    background-size: cover;
    background-position: center center;
}
</style>
<section class="section">
    <div class="section-header">
        <div class="section-header-back">
            <a href="{{ route('postingan.index') }}" class="btn btn-icon"><i class="fas fa-arrow-left"></i></a>
        </div>
        <h1>Create New Post</h1>
        <div class="section-header-breadcrumb">
            <div class="breadcrumb-item active"><a href="{{ route('member.dashboard') }}">Dashboard</a></div>
            <div class="breadcrumb-item"><a href="{{ route('postingan.index') }}">Postingan</a></div>
            <div class="breadcrumb-item">Create New Post</div>
        </div>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="card-header">
                            <h4>Project Detail</h4>
                        </div>
                        <div class="card card-warning">
                            <br>
                            <form action="{{ route('post.update', $post->id) }}" method="POST" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Title</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="title" class="form-control" value="{{ $post->title }}">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Description</label>
                                    <div class="col-sm-12 col-md-7">
                                        <textarea class="summernote-simple" name="description">{{ $post->description }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Logo</label>
                                    <div class="col-sm-12 col-md-7">
                                        <div id="image-preview" class="image-preview">
                                            <label for="image-upload" id="image-label">Choose File</label>
                                            <input type="file" name="logo" id="image-upload" value="{{ $post->logo }}" />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">&nbsp;</label>
                                    <div class="col-sm-12 col-md-7">
                                        <span class="text-danger"> Note : Size 300px x 300px </span>
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Website</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="website" class="form-control" value="{{ $post->website }}" placeholder="Website Link">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Telegram</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="telegram" value="{{ $post->telegram }}" placeholder="Telegram Channel Link" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Twitter</label>
                                    <div class="col-sm-12 col-md-7">
                                        <input type="text" name="twitter" value="{{ $post->twitter }}" class="form-control" placeholder="Twitter Link">
                                    </div>
                                </div>
                                <div class="form-group row mb-4">
                                    <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Status</label>
                                    <div class="col-sm-12 col-md-7">
                                        <select class="form-control selectric" name="status">
                                            <option value="1" {{ ($post->status == 1 ) ? 'selected': '' }}>Publish</option>
                                            <option value="2" {{ ($post->status == 2 ) ? 'selected': '' }}>Draft</option>
                                            <option value="3" {{ empty($post->status) ? 'selected': '' }}>Pending</option>
                                        </select>
                                    </div>
                                </div><br>
                                <br>
                                <div class="card-header">
                                    <h4>Airdrop Detail</h4>
                                </div>
                                <div class="card card-warning">
                                    <br>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Airdrop Link</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="text" name="airdrop_link" class="form-control" value="{{ $airdrop->airdrop_link }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Airdrop Start</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="date" name="airdrop_starts" value="{{ Carbon\Carbon::parse($airdrop->airdrop_starts)->format('Y-m-d') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Airdrop Ends</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="date" name="airdrop_ends" value="{{ Carbon\Carbon::parse($airdrop->airdrop_ends)->format('Y-m-d') }}" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Airdrop Distribution</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="date" name="airdrop_distribution" value="{{ Carbon\Carbon::parse($airdrop->airdrop_distribution)->format('Y-m-d') }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <br>
                                <div class="card-header">
                                    <h4>For Participants</h4>
                                </div>
                                <div class="card card-warning">
                                    <br>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Rewards</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="rewards" class="form-control" placeholder="According your token" value="{{ !empty($participants->rewards) ? $participants->rewards : 0 }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Estimated Value</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="value_rewards" class="form-control" value="{{ !empty($participants->value_rewards) ? $participants->value_rewards : 0 }}" placeholder="Estimated Rewards in USD">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Referall Bonus</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="referral_bonus" class="form-control" placeholder="According your token" value="{{ !empty($participants->referral_bonus) ? $participants->referral_bonus : 0 }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Estimated Value</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="value_referral" class="form-control" value="{{ !empty($participants->value_referral) ? $participants->value_referral : 0 }}" placeholder="Estimated Referral in USD">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3">Maximum Participants</label>
                                        <div class="col-sm-12 col-md-7">
                                            <input type="number" name="maximum_participant" class="form-control" value="{{ !empty($participants->maximum_participant) ? $participants->maximum_participant : 0 }}">
                                        </div>
                                    </div>
                                    <div class="form-group row mb-4">
                                        <label class="col-form-label text-md-right col-12 col-md-3 col-lg-3"></label>
                                        <div class="col-sm-12 col-md-7">
                                            <button class="btn btn-primary">Save Post</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('js')

<script>
$( document ).ready(function(event) {
   console.log( event );
  
});
 
</script>
<!-- JS Libraies -->
<script src="{{ asset('assets/bundles/summernote/summernote-bs4.js') }}"></script>
<script src="{{ asset('assets/bundles/jquery-selectric/jquery.selectric.min.js') }}"></script>
<script src="{{ asset('assets/bundles/upload-preview/assets/js/jquery.uploadPreview.min.js') }}"></script>
<!-- Page Specific JS File -->
<script src="{{ asset('assets/js/page/create-post.js') }}"></script>
@endsection