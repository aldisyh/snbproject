<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|  
*/

Route::get('/', 'Landing\HomeController@index')->name('landing.home');
Route::get('/login', 'Auth\LoginController@formlogin')->name('login');


Route::get('/airdrop/{slug}', 'Landing\HomeController@detail_airdrop')->name('landing.detail_airdrop');

Route::get('/airdrop/update_koin', 'Landing\HomeController@update_koin')->name('airdrop.update_koin');


Route::group([
    'prefix' => 'auth',
], function () {
    Route::get('/login', 'Auth\LoginController@formlogin')->name('member.form');
    Route::post('/login', 'Auth\LoginController@postLogin')->name('member.login');
    Route::get('/logout', 'Auth\LoginController@logout')->name('member.logout');
    Route::get('/admin/logout', 'Admin\Auth\LoginController@logout')->name('logout');
    Route::get('/form-register', 'Member\Auth\LoginController@formregister')->name('register');
    Route::post('/register', 'Member\Auth\LoginController@register')->name('post.register');

    Route::get('email/verify/{token}', 'Member\Auth\LoginController@verify_email')->name('email.verify_email');
    
    Route::get('email/forgot', 'Member\Auth\LoginController@forgot_email')->name('email.forgot_email');
    Route::post('email/forgot', 'Member\Auth\LoginController@post_forgot_email')->name('email.post_forgot_email');
    Route::get('email/forgot/{token}', 'Member\Auth\LoginController@forgot_email_reset')->name('email.forgot_email_reset');

    Route::post('email/reset/{id}', 'Member\Auth\LoginController@reset_account')->name('email.reset_account');

});


Route::group([
    'prefix' => 'admin',
    'middleware' => 'admin',
], function () {
    Route::get('/home', 'Admin\HomeController@index')->name('admin.home');

    Route::group([
        'prefix' => 'post',
    ], function () {
        Route::get('/index', 'Admin\Post\PostController@index')->name('post.index');
        Route::get('/form', 'Admin\Post\PostController@form')->name('post.form');
        Route::get('/detail/{id}', 'Admin\Post\PostController@detail_post')->name('post.detail');
        Route::get('/edit_post/{id}', 'Admin\Post\PostController@edit')->name('post.edit');
        Route::post('/update_post/{id}', 'Admin\Post\PostController@update')->name('post.update');

        //status
        Route::get('/publish/{id}', 'Admin\Post\PostController@publish')->name('publish.post');
        Route::get('/draft/{id}', 'Admin\Post\PostController@draft')->name('draft.post');
        Route::get('/delete/{id}', 'Admin\Post\PostController@delete')->name('delete.post');

        //is_status
        Route::get('/new/{id}', 'Admin\Post\PostController@new')->name('new.post');
        Route::get('/featured/{id}', 'Admin\Post\PostController@featured')->name('featured.post');
        Route::get('/ended/{id}', 'Admin\Post\PostController@ended')->name('ended.post');
    });

    Route::group([
        'prefix' => 'user',
    ], function () {
        Route::get('/index', 'Admin\User\UserController@index')->name('user.index');
    });
});



Route::group([
    'prefix' => 'member',
    'middleware' => 'member',
], function () {
    Route::get('/dashboard', 'Member\HomeController@index')->name('member.dashboard');

    Route::group([
        'prefix' => 'post',
    ], function () {
        Route::get('/index', 'Member\Post\PostController@index')->name('postingan.index');
        Route::get('/form', 'Member\Post\PostController@form')->name('postingan.form');
        Route::post('/tambah', 'Member\Post\PostController@tambah')->name('postingan.create');
        Route::get('/detail/{id}', 'Member\Post\PostController@postdetail')->name('detail.post');
    });

    Route::group([
        'prefix' => 'akun',
    ], function () {
        Route::get('/profile', 'Member\Profile\ProfileController@index')->name('profile.index');
        Route::post('/changepassword', 'Member\Profile\ProfileController@changepassword')->name('change.password');
    });
});
