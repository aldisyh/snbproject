<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class RoleUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("CREATE VIEW role_users AS
        SELECT 
            u.*,ar.role_id , ar.user_id, r.nm_role
        FROM
            users u, access_role ar, `role` r
        where 
        ar.user_id = u.id AND  ar.role_id = r.id;");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement("DROP VIEW role_users");
    }
}
