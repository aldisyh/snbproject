<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAirdropDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('airdrop_detail', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('airdrop_link');
            $table->string('airdrop_starts');
            $table->string('airdrop_ends');
            $table->string('airdrop_distribution');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('airdrop_detail');
    }
}
